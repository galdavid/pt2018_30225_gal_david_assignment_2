package queue;

import javax.swing.JLabel;

public class Client {
	
	protected int timpsosire;
	protected int timpservire;
	public String label;

	public Client( int timpsosire, int timpservire,int numar){
		
		this.timpsosire = timpsosire;
		this.timpservire = timpservire;
		this.label=new String(" "+numar+" ");
	}
	public int gettimpsosire(){
		
		return timpsosire;
	}
	public int gettimpservire(){
		
		return timpservire;
	}
	public void setTimpserivre(int time){
		
		this.timpservire = time;
	}
	public String toString(int time, int nr){
		
		String w ="";
		w += time;
		w="Client("+(nr+1)+")Time:" + w + " ";
		return w;
	}
}