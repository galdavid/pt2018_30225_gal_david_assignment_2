package queue;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class Interfata extends JFrame{
	
	public static JTextField[] coada;
	private JFrame jf;
	private JLabel tsosmin,atsosmin,tsosmax,atsosmax,tservmin,atservmin,tservmax,atservmax,timp,timpmed,nrclient,nrcasa;
	private JTextField somin,somax,servmin,servmax,anrclient,nrcasaa;
	static JTextField timpmediu,itime;
	private JButton pornire,enter;
	protected thread thred;
	public static int nrcli,nrcli1,minsos,maxsos,minser,maxser;
	
	public Interfata(){
		
		jf = new JFrame("Tema3");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setBounds(200, 30, 900, 700);
		jf.getContentPane().setLayout(null);
		
		
		atsosmin = new JLabel();
		atsosmin.setText("Min:");
		atsosmin.setBounds(150, 40, 100, 20);
		jf.getContentPane().add(atsosmin);
		
		somin = new JTextField();
		somin.setBounds(250, 40, 40, 30);
		jf.getContentPane().add(somin);
		
		tsosmax = new JLabel();
		tsosmax.setText("Timp cand a ajuns:");
		tsosmax.setBounds(40, 20, 100, 20);
		jf.getContentPane().add(tsosmax);
		
		atsosmax = new JLabel();
		atsosmax.setText("Max:");
		atsosmax.setBounds(150, 70, 100, 20);
		jf.getContentPane().add(atsosmax);
		
		somax = new JTextField();
		somax.setBounds(250, 70, 40, 30);
		jf.getContentPane().add(somax);
		
		tservmin = new JLabel();
		tservmin.setText("Timp de servire:");
		tservmin.setBounds(350, 20, 100, 20);
		jf.getContentPane().add(tservmin);
		
		atservmin = new JLabel();
		atservmin.setText("Min:");
		atservmin.setBounds(460, 40, 100, 20);
		jf.getContentPane().add(atservmin);
		
		servmin = new JTextField();
		servmin.setBounds(540, 40, 40, 30);;
		jf.getContentPane().add(servmin);
		
		tservmax = new JLabel();
		tservmax.setText("Timp de servire");
		tservmax.setBounds(350, 20, 100, 20);
		jf.getContentPane().add(tservmax);
		
		atservmax = new JLabel();
		atservmax.setText("Max:");
		atservmax.setBounds(460, 70, 100, 20);
		jf.getContentPane().add(atservmax);
		
		servmax = new JTextField();
		servmax.setBounds(540, 70, 40, 30);
		jf.getContentPane().add(servmax);
		
		timp = new JLabel();
		timp.setText("Timp:");
		timp.setBounds(40, 150, 100, 20);
		jf.getContentPane().add(timp);
		
		itime = new JTextField();
		itime.setBounds(110, 150, 70, 30);
		itime.setEditable(false);
		jf.getContentPane().add(itime);
		
		timpmed = new JLabel();
		timpmed.setText("Timp mediu de asteptare");
		timpmed.setBounds(200, 150, 150, 20);
		jf.getContentPane().add(timpmed);
		
		timpmediu = new JTextField();
		timpmediu.setEditable(false);
		timpmediu.setBounds(370, 150, 70, 30);
		jf.getContentPane().add(timpmediu);
		
		nrclient = new JLabel();
		nrclient.setText("Cumparator:");
		nrclient.setBounds(40, 190, 100, 20);
		jf.getContentPane().add(nrclient);
		
		anrclient = new JTextField();
		anrclient.setBounds(110, 190, 70, 30);
		jf.getContentPane().add(anrclient);
		
		nrcasa = new JLabel();
		nrcasa.setText("Nr casa:");
		nrcasa.setBounds(295, 190, 100, 20);
		jf.getContentPane().add(nrcasa);
		
		nrcasaa = new JTextField();
		nrcasaa.setBounds(370, 190, 70, 30);
		jf.getContentPane().add(nrcasaa);
		
		pornire = new JButton("Pornire");
		pornire.setBounds(710, 200, 70, 30);
		jf.getContentPane().add(pornire);
		
		enter = new JButton("Enter");
		enter.setBounds(550, 200, 150, 30);
		jf.getContentPane().add(enter);
		
		jf.setVisible(true);
		
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				if(e.getSource() == enter){
					nrcli = Integer.parseInt(nrcasaa.getText());
					if (nrcli <= 6) {
						coada = new JTextField[nrcli];
						for(int i=0; i<nrcli;i++){
							coada[i] = new JTextField();
							coada[i].setText(" ");
							coada[i].setBounds(100,250+50*i,500,50);;
							coada[i].setVisible(true);
							jf.getContentPane().add(coada[i]);
							
						}
					}
					else
						JOptionPane.showMessageDialog(null,"Locure de casa maxim 6!!!" );
					
				}
			}
		});
		
		pornire.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == pornire){
					minsos = Integer.parseInt(somin.getText());
					maxsos = Integer.parseInt(somax.getText());
					minser = Integer.parseInt(servmin.getText());
					maxser = Integer.parseInt(servmax.getText());
					nrcli = Integer.parseInt(nrcasaa.getText());
					nrcli1 = Integer.parseInt(anrclient.getText());
					ArrayList<Client> clnt = new ArrayList<Client>();
					int timpsosire,timpservire;
					System.out.printf("Casa %d \t Cumparator: %d \nMin ajunge: %d \t Max ajunge: %d \nMin servire: %d \t Max Servire : %d\n",nrcli,nrcli1,minsos,maxsos,minser,maxser);
					for(int i=0;i<nrcli1;i++){
						timpsosire = randomSpace(minsos,maxsos);
						timpservire = randomSpace(minser,maxser);
						Client c1 = new Client(timpsosire,timpservire,i+1);
						clnt.add(c1);
					}
					thred = new thread(nrcli1,nrcli);
					Thread th = new Thread(thred);
					th.start();
				}
			}
		});
		
	}

	private int randomSpace(int x,int y){
		Random rnd = new Random();
		int n;
		do { 
			n = rnd.nextInt(10000);
		}while((n<x)|| (n>y));
		return n ;
	}
}
